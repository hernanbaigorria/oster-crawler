$(document).ready(function(){
	 $('#list').DataTable({"bSort": false});
	 $('#list2').DataTable({"bSort": false});
	 $('#list3').DataTable({"bSort": false});
	 $('#list4').DataTable({"bSort": false});
	 $('#list5').DataTable({"bSort": false});
	 
	
	 var editor = new wysihtml5.Editor("wysihtml5-textarea", { 
	  toolbar:      "wysihtml5-toolbar",
	  parserRules: wysihtml5ParserRules
	});
	 var editor = new wysihtml5.Editor("wysihtml5-textarea2", { 
	  toolbar:      "wysihtml5-toolbar2",
	  parserRules: wysihtml5ParserRules
	});
	
	/*CAMPOS A VALIDAR*/
	jsonDatos=eval('({"campos":['+
		'{"campo":"titulo","validacion":"B"},'+
		'{"campo":"wysihtml5-textarea","validacion":"B"}'+			
		']})');
	
	/*UPLOAD CONFIG*/
	var w=1440;
	var h=810;
	var path='../../../../asset/img/uploads/';
	var maxWidth=1440;
	var thWidth=1440;
	var thHeight=810;		
	var tipo='unica';
	var allowedTypes='jpg,png,gif'
	var callback=function(){console.log('upload complete');}

	uploaderwysihtml5('wysihtml5',path,maxWidth,thHeight,thWidth,tipo,5,allowedTypes,true,callback);
	uploaderwysihtml5('wysihtml52',path,maxWidth,thHeight,thWidth,tipo,5,allowedTypes,true,callback);
	
	var maxWidth=1440;
	var thWidth=1440;
	var thHeight=810;		
	var tipo='unica';
	var allowedTypes='jpg,png,gif';
	var callback=function(){console.log('upload complete');}

	uploaderNoCrop('1',path,maxWidth,thHeight,thWidth,tipo,5,allowedTypes,true,callback);
	
		
	$('#categoria').change(function(){
		obtenerEstilos($(this).val(),0);
	});
	$('#estilo').change(function(){
		obtenerLineas($(this).val(),0);
	});
	obtenerEstilos($('#categoria').val(),1);
});

function obtenerEstilos(categoria, first){
	url=base_url+'productos/getestilos/?categoria='+categoria;
		$.ajax({
			url: url,
			success: function(response){	
				$('#estilo').html(response);
				if(p_accion=='edit' && first==1){
					url=base_url+'productos/getestiloproducto/?producto='+p_id;
					$.ajax({
						url: url,
						success: function(response){	
							$('#estilo').val(response);
							obtenerLineas($('#estilo').val(),1);
						}
					});
				}else{
					obtenerLineas($('#estilo').val(),0);
				}
				
			}
		});	
}
function obtenerLineas(estilo, first){
	url=base_url+'productos/getlineas/?estilo='+estilo;
		$.ajax({
			url: url,
			success: function(response){	
				$('#linea').html(response);
				if(p_accion=='edit' && first==1){
					url=base_url+'productos/getlineaproducto/?producto='+p_id;
					$.ajax({
						url: url,
						success: function(response){	
							$('#linea').val(response);
						}
					});
				}
			}
		});
	
}

