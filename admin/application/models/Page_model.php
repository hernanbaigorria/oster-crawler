<?php

class page_model extends CI_Model
{


	public function __construct()
        {
		parent::__construct();
		// Your own constructor code
        }
        
        
        public function get_sorteo_local($local_id)
        {       
            $this->db->select("participantes.*");
            $this->db->select("locales.id as id_local");

            $this->db->join('participantes', 'participantes.id = participantes_cupones.participante_id', 'inner');
            $this->db->join('locales', 'locales.id = participantes.nombre_sucursal', 'inner');
            
            $this->db->where("locales.id",$local_id);
            $this->db->limit(10);
            
            $this->db->group_by('participantes.dni');
            $this->db->order_by('participantes.id','RANDOM');
            
            $query = $this->db->get('participantes_cupones');
            return $query->result();

        }
        
        public function get_localidades_para_local()
        {
			$this->db->select('localidades.*');
			$this->db->select("provincias.nombre as nombre_provincia");
			
			$this->db->join("provincias","provincias.id = localidades.provincia_id","inner");
			
			$this->db->from('localidades');
			$this->db->order_by("provincias.nombre,localidades.localidad","asc");
			
			$query = $this->db->get();
			
			return $query->result();
        }
        
        public function get_localidades_para_local_by_id($id)
        {
			$this->db->select('localidades.*');
			$this->db->select("provincias.nombre as nombre_provincia");
			
			$this->db->join("provincias","provincias.id = localidades.provincia_id","inner");
			
			$this->db->where("localidades.id",$id);
			
			$this->db->from('localidades');
			$this->db->order_by("provincias.nombre,localidades.localidad","asc");
			
			$query = $this->db->get();
			
			return $query->result();
        }

        public function get_products()
        {
            $query = $this->db->get('mainArts');
            return $query->result();
        }
        
        public function get_participantes_local($id)
        {
            $this->db->select('participantes.*');
            //$this->db->select("provincias.nombre as nombre_provincia");
            
            //$this->db->join("provincias","provincias.id = localidades.provincia_id","inner");
            
            $this->db->where("participantes.nombre_sucursal",$id);
            
            $this->db->from('participantes');
            $this->db->order_by('participantes.id','RANDOM');
            
            $query = $this->db->get();
            
            return $query->result();
        }

        public function get_countries()
        {
            $query = $this->db->get('countries');
            return $query->result();
        }

        public function get_retails()
        {
            $this->db->select('retails.*');
            $this->db->select('countries.country');
            $this->db->join('countries', 'countries.id = retails.id_country');
            $query = $this->db->get('retails');
            return $query->result();
        }
        
        public function login($data) {

		$condition = "user_login.user_name =" . "'" . $data['username'] . "' AND " . "user_login.user_password =" . "'" . $data['password'] . "'";
		$this->db->select('user_login.*');
		$this->db->from('user_login');
		$this->db->where($condition);
		$this->db->limit(1);
		$query = $this->db->get();

		if ($query->num_rows() == 1) {
			return true;
		} else {
			return false;
		}
		}
	public function checkuser($user) {

		$condition = "user_name =" . "'" . $user . "'";
		$this->db->select('*');
		$this->db->from('user_login');
		$this->db->where($condition);
		$this->db->limit(1);
		$query = $this->db->get();

		if ($query->num_rows() == 1) {
			return true;
		} else {
			return false;
		}
	}

	// Read data from database to show data in admin page
	public function read_user_information($username) {
		$condition = "user_name =" . "'" . $username . "'";
		$this->db->select('user_login.*');
		$this->db->from('user_login');
		$this->db->where($condition);
		$this->db->limit(1);
		$query = $this->db->get();

		if ($query->num_rows() == 1) {
			return $query->result();
		} else {
			return false;
		}
	}
			
	public function get_banners()
        {
		$this->db->select('banner.*, provincia.provincia');
		if($this->session->userdata['logged_in']['administrator']==0){
			$this->db->where('sede.provinciaId',$this->session->userdata['logged_in']['provinciaId']);
		}
		$this->db->join('provincia', 'provincia.id = banner.provinciaId', 'left');
                $query = $this->db->get('banner');
                return $query->result();
        }
		public function get_banner_id($id)
        {
				$this->db->where('id', $id);
                $query = $this->db->get('banner');
                return $query->result();
        }	

        public function get_categorias_id($id)
        {
		$this->db->where('id', $id);
                $query = $this->db->get('categorias');
                return $query->result();
        }

        public function get_sub_categorias_id($id)
        {
                $this->db->where('parent_id', $id);
                $query = $this->db->get('categorias');
                return $query->result();
        }


        public function get_parent_id($id)
        {
                $this->db->where('id', $id);
                $query = $this->db->get('categorias');
                return $query->result();
        }

        public function get_categorias_productos($id_parent, $html)
        {       
                $this->db->where('parent_id', $id_parent);
                $query = $this->db->get('categorias');
                foreach($query->result() as $fila):
                    if(!empty($fila->{'id'})):
                        $html.='<option value="'.$fila->{'id'}.'">'.$fila->{'nombre'}.'</option>';
                        $html.= $this->get_categorias_productos($fila->{'id'}, '');
                    endif;
                endforeach;
                return $html;
        }

        public function get_filter_productos($id_parent, $html, $id_product)
        {       
                $this->db->select('categorias.*');
                $this->db->select('filtros_productos.id_producto as id_producto');

                $this->db->group_by('nombre');

                $this->db->where('categorias.parent_id', $id_parent);

                $this->db->join('filtros_productos', 'filtros_productos.id_categoria = categorias.id', 'left');

                $query = $this->db->get('categorias');

                
                foreach($query->result() as $fila):
                    print_r($fila);
                    //exit;
                    if($id_product == $fila->{'id_producto'}):
                        $html.='<option value="'.$fila->{'id'}.'" selected>'.$fila->{'nombre'}.'</option>';
                        $html.= $this->get_filter_productos($fila->{'id'}, '', $id_product);
                    else:
                    $html.='<option value="'.$fila->{'id'}.'">'.$fila->{'nombre'}.'</option>';
                    $html.= $this->get_filter_productos($fila->{'id'}, '', $id_product);
                    endif;
                    
                endforeach;
                return $html;
        }
		    
	    public function get_categorias()
        {       
                $this->db->where('parent_id', NULL);
                $query = $this->db->get('categorias');
                return $query->result();
        }

        public function get_categorias_structure($id_parent, $html)
        {       
                $this->db->where('parent_id', $id_parent);
                $query = $this->db->get('categorias');
                $html.='<ul class="lis-'.$id_parent.'">';
                foreach($query->result() as $fila):
                        $html.='<li class="'.$id_parent.'">'.$fila->{'nombre'}.'
                        <a style="margin-left:30px;" href='.base_url().'productos/add_sub_categoria/?catid='.$fila->{'id'}.'/><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></a>
                        <a href='.base_url().'productos/edit_categoria/?catid='.$fila->{'id'}.'/><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>
                        <a href="#" data-href="'.base_url().'productos/delete_categoria/?catid='.$fila->{'id'}.'&parentid='.$fila->{'parent_id'}.'"/ data-toggle="modal" data-target="#confirm-delete"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
                        </li>';
                        $html.= $this->get_categorias_structure($fila->{'id'}, '');
                endforeach;
                $html.='</ul>';
                return $html;
        }

        public function get_sub_categorias($id_categoria)
        {		
        		$this->db->where('id_categoria', $id_categoria);
                $query = $this->db->get('categoria_sub');
                return $query->result();
        }

        public function get_sub_sub_categorias($id_categoria)
        {		
		$this->db->where('categoria_sub_sub.id_categoria_sub', $id_categoria);

                $this->db->select('categoria_sub_sub.id as id');
		$this->db->select('categoria_sub.id as id_sub_categoria');
		$this->db->select('categorias.id as id_categoria');
		$this->db->select('categoria_sub_sub.nombre as nombre_sub_sub');

		$this->db->join('categoria_sub', 'categoria_sub.id = categoria_sub_sub.id_categoria_sub', 'left');
		$this->db->join('categorias', 'categorias.id = categoria_sub_sub.id_categoria', 'left');

                $query = $this->db->get('categoria_sub_sub');
                return $query->result();
        }

        public function get_categorias_sorteos()
        {
                $query = $this->db->get('categorias_sorteos');
                return $query->result();
        }


        public function get_leads_provincias()
        {       
            $this->db->select("provincias.nombre as provincia");
            $this->db->select("count(*) AS count");
            $this->db->group_by('provincia_sucursal');
            $this->db->join('provincias', 'provincias.id = participantes.provincia_sucursal', '');
            $this->db->order_by('count','desc');
            $query = $this->db->get('participantes');

            return $query->result();

        }

        public function get_cupones_provincias()
        {       
            $this->db->select("provincias.nombre as provincia");
            $this->db->select("count(*) AS count");
            $this->db->group_by('participantes.provincia_sucursal');
            $this->db->order_by('count','desc');
            $this->db->join('participantes', 'participantes.id = participantes_cupones.participante_id', 'left');
            $this->db->join('provincias', 'provincias.id = participantes.provincia_sucursal', '');
            $query = $this->db->get('participantes_cupones');

            return $query->result();

        }

        public function get_cupones_local()
        {       
            $this->db->select("count(*) AS count");
            $this->db->select("locales.id as id_local");
            $this->db->select("locales.nombre as local");
            $this->db->select("localidades.localidad as localidad");
            $this->db->select("provincias.nombre as provincia");
            $this->db->group_by('local');
            $this->db->order_by('count','desc');
            $this->db->join('participantes', 'participantes.id = participantes_cupones.participante_id', 'right');
            $this->db->join('locales', 'locales.id = participantes.nombre_sucursal', '');
            $this->db->join('localidades', 'localidades.id = locales.localidad_id', 'left');
            $this->db->join('provincias', 'provincias.id = localidades.provincia_id', 'left');
            $query = $this->db->get('participantes_cupones');
            return $query->result();

        }


        public function get_cupones_no_local()
        {       
            $this->db->select("*");
            $this->db->select("locales.nombre as local");
            $this->db->select("localidades.localidad as localidad");
            $this->db->select("provincias.nombre as provincia");
            
            $this->db->where("locales.id not in (select nombre_sucursal from participantes group by nombre_sucursal)");

            $this->db->join('localidades', 'localidades.id = locales.localidad_id', 'left');
            $this->db->join('provincias', 'provincias.id = localidades.provincia_id', 'left');

            $query = $this->db->get('locales');

            return $query->result();
        }


        public function get_cupones_promedio()
        {       
            $this->db->select("count(*) AS count");
            
            $this->db->select("participantes.*");
            $this->db->group_by('participantes.dni');
            $this->db->order_by('count','desc');
            $this->db->join('participantes', 'participantes.id = participantes_cupones.participante_id', 'left');
            $query = $this->db->get('participantes_cupones');

            return $query->result();

        }


        public function get_promociones()
        {
                $query = $this->db->get('promociones');
                return $query->result();
        }

        public function get_perfiles()
        {
                $query = $this->db->get('lead');
                return $query->result();
        }

        public function get_participantes($pais)
        {   
            if($pais != NULL){
                $this->db->where('participantes.pais', $pais);
                $this->db->limit(3);
            }
            $this->db->order_by('participantes.id', 'random');
            $query = $this->db->get('participantes');
            return $query->result();
        }

        public function get_participantes_promocion()
        {   
            $this->db->order_by('promocion.fecha_registro', 'asc');
            $query = $this->db->get('promocion');
            return $query->result();
        }

        public function get_participantes_naviadad($pais)
        {   

            if($pais != NULL){
                $this->db->where('navidad.pais', $pais);
                $this->db->limit(3);
            }
            $this->db->order_by('navidad.id', 'asc');
            $query = $this->db->get('navidad');
            return $query->result();
        }
        
        public function get_participantes_clases($pais)
        {   

            if($pais != NULL){
                $this->db->where('clases.pais', $pais);
                $this->db->limit(3);
            }
            $this->db->order_by('clases.id', 'asc');
            $query = $this->db->get('clases');
            return $query->result();
        }

        public function get_participantes_multimama($pais)
        {   

            if($pais != NULL){
                $this->db->where('multimama.pais', $pais);
                $this->db->limit(3);
            }
            $this->db->order_by('multimama.id', 'asc');
            $query = $this->db->get('multimama');
            return $query->result();
        }

        public function get_participantes_navidad_2021($pais)
        {   

            if($pais != NULL){
                $this->db->where('navidad_2021.pais', $pais);
                $this->db->limit(3);
            }
            $this->db->order_by('navidad_2021.id', 'asc');
            $query = $this->db->get('navidad_2021');
            return $query->result();
        }

        public function get_participantes_summer_2022($pais)
        {   

            if($pais != NULL){
                $this->db->where('verano_refrescante_2022_costa_rica.pais', $pais);
                $this->db->limit(3);
            }
            $this->db->order_by('verano_refrescante_2022_costa_rica.id', 'asc');
            $query = $this->db->get('verano_refrescante_2022_costa_rica');
            return $query->result();
        }
        
        // ---        
        // Getting cupones canjeados
        // ---

        
        public function get_locales()
        {
        	$this->db->select("*");
            $query = $this->db->get('locales');
            return $query->result();
        }
        
        
        
        public function get_localidades()
        {   
        	$this->db->select("localidades.*");
        	$this->db->where("localidades.provincia_id",$this->session->userdata('provincia_id'));
            $query = $this->db->get('localidades');
            return $query->result();
        }
		
		
		public function existeCupon($cupon)
		{
			$this->db->where("cupon",$cupon);
			$query = $this->db->get("cupones");
			if ($query->num_rows() >= 1) {		
			
				return true;
			}
			return false;
		}
		
		
        public function get_cupones()
        {		
			$this->db->select('cupones.*');
                $query = $this->db->get('cupones');
                return $query->result();
        }

        public function get_sorteos()
        {		
		$this->db->select('sorteo.*');

		$this->db->select('categorias_sorteos.nombre as categoria');
		$this->db->select('lead.nombre as nombre_lead');

		$this->db->join('categorias_sorteos','categorias_sorteos.id = sorteo.id_categoria');
		$this->db->join('lead','lead.id = sorteo.id_lead');

                $query = $this->db->get('sorteo');
                return $query->result();
        }

        public function get_producto_id()
        {
        	$this->db->where('id', $this->uri->segment(3));
        	$query = $this->db->get('productos');
        	return $query->result();	
        }

        public function get_marca_id()
        {
            $this->db->where('id', $this->uri->segment(3));
            $query = $this->db->get('marcas');
            return $query->result();    
        }
        
        public function get_locales_id()
        {
            $this->db->where('id', $this->uri->segment(3));
            $query = $this->db->get('locales');
            return $query->result();    
        }
        

        public function get_country_id()
        {
            $this->db->where('id', $this->uri->segment(3));
            $query = $this->db->get('countries');
            return $query->result();    
        }

        public function get_retail_id()
        {
            $this->db->where('id', $this->uri->segment(3));
            $query = $this->db->get('retails');
            return $query->result();    
        }
        
        public function get_provincia_id()
        {
            $this->db->where('id', $this->uri->segment(3));
            $query = $this->db->get('provincias');
            return $query->result();    
        }
        
        public function get_provincia_id_id($id)
        {
            $this->db->where('id', $id);
            $query = $this->db->get('provincias');
            return $query->result();    
        }

        public function get_promociones_id()
        {
	       $this->db->where('id', $this->uri->segment(3));
	       $query = $this->db->get('promociones');
	       return $query->result();	
        }

	public function get_categoria_id($id)
        {
		$this->db->where('id', $id);
                $query = $this->db->get('categorias');
                return $query->result();
        }

        public function get_sub_categoria_id($id)
        {
                $this->db->where('id', $id);
                $query = $this->db->get('categoria_sub');
                return $query->result();
        }

        public function get_sub_sub_categoria_id($id)
        {
                $this->db->where('id', $id);
                $query = $this->db->get('categoria_sub_sub');
                return $query->result();
        }

 
	public function get_active_provinces()
        {
		$this->db->select('provincia.*');
		if($this->session->userdata['logged_in']['administrator']==0){
			$this->db->where('provincia.id',$this->session->userdata['logged_in']['provinciaId']);
		}
		$this->db->join('sede','sede.provinciaId = provincia.id');
		$this->db->group_by('provincia.id');
		$query = $this->db->get('provincia');
		return $query->result();				
        }
	public function get_provincia_curso($id)
        {
		$this->db->where('cursoId', $id);
                $query = $this->db->get('curso_provincia');
                return $query->result();
        }	
		
	public function get_noticias()
        {
		$this->db->select('noticias.*');
		$this->db->order_by('id','desc');
                $query = $this->db->get('noticias');
                return $query->result();		
        }
	public function get_noticia_id($id)
        {
		$this->db->where('id', $id);	
                $query = $this->db->get('noticias');
                return $query->result();		
        }
	public function get_provincia_noticia($id)
        {
		$this->db->select('noticias_provincia.*, provincia.provincia');
		$this->db->where('noticiaId', $id);
		$this->db->join('provincia', 'provincia.id = noticias_provincia.provinciaId', 'left');
                $query = $this->db->get('noticias_provincia');
                return $query->result();
        }		
		
	public function get_usuarios()
        {
		$this->db->select('user_login.*');
                $query = $this->db->get('user_login');
                return $query->result();
        }
	public function get_usuarios_id($id)
        {
		$this->db->where('id', $id);
                $query = $this->db->get('user_login');
                return $query->result();
        }		
			
	public function get_imagenes_archivo($entidad,$id)
        {
		$this->db->where('refId', $id);
		$this->db->where('entidad', $entidad);
                $query = $this->db->get('archivos');
                return $query->result();		
        }
		
		
		
	/*INSERTS*/
	
	    public function insert_locales($data)
        {
		$this->db->insert('locales', $data);	
        }
        
	    public function insert_marcas($data)
        {
		$this->db->insert('marcas', $data);	
        }
        
        
	    public function insert_country($data)
        {
		$this->db->insert('countries', $data);	
        }

        public function insert_retail($data)
        {
        $this->db->insert('retails', $data);  
        }
        
	    public function insert_provincias($data)
        {
			$this->db->insert('provincias', $data);	
        }

        public function insert_categoria($data)
        {
        $this->db->insert('categorias', $data); 
        }

        public function insert_sub_categoria($data)
        {
		$this->db->insert('categorias', $data);	
        }

        public function insert_sub_sub_categoria($data)
        {
                $this->db->insert('categoria_sub_sub', $data);      
        }

        public function insert_categoria_sorteo($data)
        {
		$this->db->insert('categorias_sorteos', $data);	
        }
				
	    public function insert_usuario($data)
        {
                $this->db->insert('user_login', $data);
        }

        public function insert_cupon($data)
        {
                $this->db->insert('cupones', $data);
        }
        
        public function insert_producto($data)
        {
                $this->db->insert('productos', $data);
        }
		
	    public function insert_filtros($data)
        {
                $this->db->insert('filtros_productos', $data);
        }

        public function insert_promocion($data)
        {
                $this->db->insert('promociones', $data);
        }
						
		
	/*UPDATES*/
	public function update_banner($data)
        {
		$this->db->where('id', $this->uri->segment(3));
                $this->db->update('banner', $data);
        }
	public function update_categoria($data)
        {
		$this->db->where('id', $_GET['catid']);
		$this->db->update('categorias', $data);
        }

        public function update_sub_categoria($data)
        {
                $this->db->where('id', $_GET['subcatid']);
                $this->db->update('categoria_sub', $data);
        }

        public function update_sub_sub_categoria($data)
        {
                $this->db->where('id', $_GET['subsubcatid']);
                $this->db->update('categoria_sub_sub', $data);
        }
		
	public function update_usuario($data)
        {
		$this->db->set('name', $data['name']);
		$this->db->set('provinciaId', $data['provinciaId']);
		$this->db->set('administrator', $data['administrator']);
		$this->db->set('lastname', $data['lastname']);
		$this->db->set('user_email', $data['user_email']);
		if($data['user_password']<>''){
			$this->db->set('user_password', $data['user_password']);
		}
		$this->db->where('id', $this->uri->segment(3));
                $this->db->update('user_login');
        }
	public function update_producto($data)
        {
		$this->db->where('id', $this->uri->segment(3));
                $this->db->update('productos', $data);
        }

        public function update_promocion($data)
        {
		$this->db->where('id', $this->uri->segment(3));
                $this->db->update('promociones', $data);
        }
        
        public function update_locales($data)
        {
        $this->db->where('id', $this->uri->segment(3));
                $this->db->update('locales', $data);
        }

        public function update_country($data)
        {
        $this->db->where('id', $this->uri->segment(3));
                $this->db->update('countries', $data);
        }

        public function update_retail($data)
        {
        $this->db->where('id', $this->uri->segment(3));
                $this->db->update('retails', $data);
        }
        
        public function update_localidades($data)
        {
        $this->db->where('id', $this->uri->segment(3));
                $this->db->update('marcas', $data);
        }
        
        
        public function update_provincia($data)
        {
        $this->db->where('id', $this->uri->segment(3));
                $this->db->update('provincias', $data);
        }
		
		
		
	/*REMOVES*/
	
	public function remove_banner()
        {
		$this->db->where('id', $this->uri->segment(3));
                $this->db->delete('banner');
        }

        public function remove_cupon()
        {
		$this->db->where('id', $this->uri->segment(3));
                $this->db->delete('cupones');
        }
        
        public function remove_provincia()
        {
		$this->db->where('id', $this->uri->segment(3));
                $this->db->delete('provincias');
        }
        
        
        public function remove_locales()
        {
		$this->db->where('id', $this->uri->segment(3));
                $this->db->delete('locales');
        }
        
        
        public function remove_country()
        {
		$this->db->where('id', $this->uri->segment(3));
                $this->db->delete('countries');
        }

        public function remove_retail()
        {
        $this->db->where('id', $this->uri->segment(3));
                $this->db->delete('retails');
        }
		
		
	public function remove_categoria()    
        {       


                $this->db->where('id', $_GET['catid']);
                $this->db->delete('categorias');

                $this->db->where('parent_id', $_GET['catid']);
                $this->db->delete('categorias');

                

               // $this->db->delete('categorias', array('id' => $_GET['catid'], 'parent_id' => $_GET['parentid']));
        }

        public function remove_sub_categoria()    
        {
                $this->db->delete('categoria_sub_sub', array('id_categoria_sub' => $_GET['subcatid'])); 
                $this->db->delete('categoria_sub', array('id' => $_GET['subcatid'])); 
        }

        public function remove_sub_sub_categoria()    
        {
                $this->db->delete('categoria_sub_sub', array('id' => $_GET['subsubcatid'])); 
        }

        public function remove_promocion()
        {
				$this->db->where('id', $this->uri->segment(3));
                $this->db->delete('promociones');
        }
	
		
		public function remove_usuario()
        {
				$this->db->where('id', $this->uri->segment(3));
                $this->db->delete('user_login');
        }
		
		
}

?>