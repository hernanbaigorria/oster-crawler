<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Retails extends CI_Controller {
	 
	function __construct()
	{
       parent::__construct();
       // testing load model
       $this->load->model('page_model');
	   // Load form helper library
	   $this->load->helper('form');
	   $this->load->helper('url');
	   $this->load->helper('string');
	   // Load form validation library
	   $this->load->library('form_validation');

	   // Load session library
	   $this->load->library('session');
	} 
	 
	
	public function index()
	{
		// ----------------------------
		// testing templating method
		// ----------------------------
	
		//como hemos creado el grupo registro podemos utilizarlo
	    $this->template->set_template('template');
	    
		//añadimos los archivos css que necesitemoa
		$this->template->add_css('asset/css/banner.css');
		
		//añadimos los archivos js que necesitemoa
		$this->template->add_js('asset/js/banner.js?v='.time().'');

		//la sección header será el archivo views/registro/header_template
	    $this->template->write_view('header', 'layout/header');
		$this->template->write_view('nav', 'layout/nav');
	    
		//desde aquí también podemos setear el título
		$this->template->write('title', 'Oster - Crawler', TRUE);
		$this->template->write('description', 'Administrador de contenidos', TRUE);
		$this->template->write('keywords', '', TRUE);

		$CI =& get_instance();
		
		$info =  $this->page_model->get_retails();
		$data['info']=$info;
		
		//el contenido de nuestro formulario estará en views/registro/formulario_registro,
		//de esta forma también podemos pasar el array data a registro/formulario_registro
	    $this->template->write_view('content', 'layout/retails/list', $data, TRUE); 
	    
		//la sección footer será el archivo views/registro/footer_template
	    //$this->template->write_view('footer', 'layout/footer');   
	    
		//con el método render podemos renderizar y hacer que se visualice la template
	    $this->template->render();
	
		 //$this->load->view('welcome_message');
	}
	
	public function add(){
		
		//como hemos creado el grupo registro podemos utilizarlo
	    $this->template->set_template('template');
	    
		//añadimos los archivos css que necesitemoa
		$this->template->add_css('asset/css/banner.css');
		
		//añadimos los archivos js que necesitemoa
		$this->template->add_js('asset/js/banner.js?v='.time().'');

		//la sección header será el archivo views/registro/header_template
	    $this->template->write_view('header', 'layout/header');
		$this->template->write_view('nav', 'layout/nav');
	    
		//desde aquí también podemos setear el título
		$this->template->write('title', 'Oster - Crawler', TRUE);
		$this->template->write('description', 'Administrador de contenidos', TRUE);
		$this->template->write('keywords', '', TRUE);

		$CI =& get_instance();
		
		$data['countries'] = $this->page_model->get_countries();
		

		$this->template->write_view('content', 'layout/retails/add', $data, TRUE); 
		$this->template->render();
	}

	public function edit()
	{
		//como hemos creado el grupo registro podemos utilizarlo
	    $this->template->set_template('template');
	    
		//añadimos los archivos css que necesitemoa
		$this->template->add_css('asset/css/banner.css');
		
		//añadimos los archivos js que necesitemoa
		$this->template->add_js('asset/js/banner.js?v='.time().'');
		
		//la sección header será el archivo views/registro/header_template
	    $this->template->write_view('header', 'layout/header');
		$this->template->write_view('nav', 'layout/nav');
	    
		//desde aquí también podemos setear el título
		$this->template->write('title', 'Administrador', TRUE);
		$this->template->write('description', 'Administrador de contenidos', TRUE);
		$this->template->write('keywords', '', TRUE);

		$CI =& get_instance();
		
		
		$info =  $this->page_model->get_retail_id($this->uri->segment(3));	
		$data['info']=$info;
		$data['countries'] = $this->page_model->get_countries();
		
		$this->template->write_view('content', 'layout/retails/edit', $data, TRUE); 
		$this->template->render();
	}

	public function update(){
		if (isset($this->session->userdata['logged_in'])) {
		
			$data = array(
				'name' => $_POST['name'],
				'title_label' => $_POST['title_label'],
				'price_label' => $_POST['price_label'],
				'body_label' => $_POST['body_label'],
				'spec_label' => $_POST['spec_label'],
				'comments_label' => $_POST['comments_label'],
				'url' => $_POST['url'],
				'id_country' => $_POST['country_id'],
			);

			$this->page_model->update_retail($data);
			redirect('retails/');
		}else{
			redirect('login/');
		}
	}
	
	public function save(){
		if (isset($this->session->userdata['logged_in'])) {

			$data = array(
				'name' => $_POST['name'],
				'title_label' => $_POST['title_label'],
				'price_label' => $_POST['price_label'],
				'body_label' => $_POST['body_label'],
				'spec_label' => $_POST['spec_label'],
				'comments_label' => $_POST['comments_label'],
				'url' => $_POST['url'],
				'id_country' => $_POST['country_id'],
			);
			
			$this->page_model->insert_retail($data);
			
			redirect('retails/');
		}else{
			redirect('login/');
		}
	}
	
	public function remove(){
		if (isset($this->session->userdata['logged_in'])) {
			$this->page_model->remove_retail();
			redirect('retails/');
		}else{
			redirect('login/');
		}
		
	}
	
	
}