<div class="home-main col-sm-10" id="home_main">
	<div class="home-content" style="margin-top:0px; padding-top:20px;" id="adm_form">
		<div class="navbar-inner">
			<ul class="nav nav-tabs">
			  <li role="presentation" class="active"><a href="#tab1" data-toggle="tab">Datos</a></li>
			</ul>
		</div>
		<form method="post" action="<?php echo base_url()?>countries/update/<?php echo $info[0]->{'id'}?>/">
		<div class="tab-content" >
		  
		  <div class="tab-pane active" id="tab1">
			
 				<div class="td-input">
					<b>Country name:</b><br>
					<input type="text" name="country" id="country" value="<?=$info[0]->country?>">
				</div>

				<div class="td-input">
					<b>Shortname:</b><br>
					<input type="text" name="shortname" id="shortname" value="<?=$info[0]->shortname?>">
				</div>

				<div class="td-input">
					<b>Language:</b><br>
					<select name="language">
						<option>Select...</option>
						<option value="spanish" <?php if($info[0]->language == 'spanish'):echo "selected"; endif; ?>>Spanish</option>
						<option value="english" <?php if($info[0]->language == 'english'):echo "selected"; endif; ?>>English</option>
						<option value="portuguese" <?php if($info[0]->language == 'portuguese'):echo "selected"; endif; ?>>Portuguese</option>
					</select>
				</div>
 
 				<div class="td-input" style="display:none">
					<b>Descripción:</b><br>
					<div id="wysihtml5-toolbar" style="display: none;">
						<header>
							<ul class="commands">
							  <li class="command" title="Make text bold (CTRL + B)" data-wysihtml5-command="bold" href="javascript:;" unselectable="on"></li>
							  <li class="command" title="Make text italic (CTRL + I)" data-wysihtml5-command="italic" href="javascript:;" unselectable="on"></li>
							  <li class="command" title="Insert an unordered list" data-wysihtml5-command="insertUnorderedList" href="javascript:;" unselectable="on"></li>
							  <li class="command" title="Insert an ordered list" data-wysihtml5-command="insertOrderedList" href="javascript:;" unselectable="on"></li>	
							  <li class="command" title="Insert a link" data-wysihtml5-command="createLink" href="javascript:;" unselectable="on"></li>
							  <li class="command" title="Insert an image" data-wysihtml5-command="insertImage" href="javascript:;" unselectable="on" ></li>
							  <li class="command" title="Insert speech" data-wysihtml5-command="insertSpeech" href="javascript:;" unselectable="on" style="display: none;"></li>
							  <li class="action" title="Show HTML" data-wysihtml5-action="change_view" href="javascript:;" unselectable="on"></li>
							</ul>
						  </header>
						  <div data-wysihtml5-dialog="createLink" style="display: none;">
							<label>
								Link:
								<input data-wysihtml5-dialog-field="href" value="http://">
							</label>
							<a data-wysihtml5-dialog-action="save">OK</a>&nbsp;<a data-wysihtml5-dialog-action="cancel">Cancel</a>
						  </div>
						  <div data-wysihtml5-dialog="insertImage" style="display:none;">
							  <label>
								Image:
								<input data-wysihtml5-dialog-field="src" id="inpt_wysihtml5" value="">
								<div class="pull-right">
									<div id="galeriawysihtml5" ></div>
									<div id="main_uploader" >
									<div class="uploader-idwysihtml5">
									<div id="uploaderwysihtml5" align="left">
									<input id="uploadifywysihtml5" type="file" class="uploader" />
									</div>
									</div>
									<div id="filesUploaded" style="display:none;"></div>
									
									<div id="thumbHeightwysihtml5" style="display:none;" >960</div>
									<div id="thumbWidthwysihtml5" style="display:none;" >1400</div>
									</div>
								</div>
								
							  </label>
							  <a data-wysihtml5-dialog-action="save">OK</a>&nbsp;<a data-wysihtml5-dialog-action="cancel">Cancel</a>
						</div>
					</div>
					<textarea id="wysihtml5-textarea" name="descripcion"></textarea>
				</div>
		  </div>	  
	   </div>
	   </form>
	   <div class="btn btn-success btn-sm pull-right bt-save" style="margin-right:8px;">SAVE</div>
	   <a href="<?php echo base_url()?>countries/"><div class="btn btn-default btn-sm pull-right" style="margin-right:8px;">CANCEL</div></a>
	</div>
</div>
<br style="clear:both;"/>
