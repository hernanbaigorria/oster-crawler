<?php
if ($this->session->userdata['logged_in']['administrator']==0) {
	header("location: ".base_url());
}
?>
<div class="home-main col-sm-10" id="home_main">
	<div class="home-content" style="margin-top:0px; padding-top:20px;">
		<div class="navbar-inner">
			<ul class="nav nav-tabs">
			  <li role="presentation" class="active"><a href="#tab1" data-toggle="tab">Datos</a></li>
			  <!--<li role="presentation"><a href="#tab2" data-toggle="tab">Subtabla relacionada</a></li>-->
			</ul>
		</div>
		<div class="tab-content" id="adm_form">
		  <div class="tab-pane active" id="tab1">
			 <form method="post" action="<?php echo base_url()?>users/update/<?php echo $info[0]->{'id'}?>/">
				<div class="td-input">
					<b>User:</b><br>
					<input type="text" name="usuario" id="usuario" value="<?php echo $info[0]->{'user_name'};?>" readonly>
				</div>
				<div class="td-input">
					<b>Name:</b><br>
					<input type="text" name="nombre" id="nombre" value="<?php echo $info[0]->{'name'};?>">
				</div>
				<div class="td-input">
					<b>Lastname:</b><br>
					<input type="text" name="apellido" id="apellido" value="<?php echo $info[0]->{'lastname'};?>">
				</div>
				<div class="td-input" style="padding-top:14px;">
					<b>Password:</b> &nbsp;<div class="btn btn-primary btn-xs" onclick="$('#password').show(); $(this).hide();">Cambiar password</div>
					<input type="text" name="password" id="password" placeholder="Ingrese nuevo password" style="display:none;" value="">
				</div>
				<div class="td-input">
					<b>Email:</b><br>
					<input type="text" name="email" id="email" value="<?php echo $info[0]->{'user_email'};?>">
				</div>
				<div class="td-input">
					<b>Admin?:</b><br>
					<?php if($info[0]->{'administrator'}==1){?>
						<input type="checkbox" name="administrator" id="administrator" checked>
					<?php }else{ ?>
						<input type="checkbox" name="administrator" id="administrator">
					<?php } ?>
				</div>
			 </form>
		  </div>
		  <div class="tab-pane" id="tab2">
			 iframe listado subtabla
		  </div>
	   </div>
	   <div class="btn btn-success btn-sm pull-right bt-save" style="margin-right:8px;">SAVE</div>
	   <a href="<?php echo base_url()?>users/"><div class="btn btn-default btn-sm pull-right" style="margin-right:8px;">CANCEL</div></a>
	</div>
</div>
<br style="clear:both;"/>