<div class="home-main col-sm-10" id="home_main">
	<div class="home-content" style="margin-top:0px; padding-top:10px;">
		<div class="listado">
			<div class="col-md-12 home-tools">
			<div class="col-md-8">
				<h2>PRODUCTS</h2>
			</div>
			<div class="col-md-4">
			</div>
			</div>
			<table id="list" class="table table-striped table-bordered dataTable" width="100%" cellspacing="0">
				<thead>
					<tr>
						<th width="40">ID</th>
						<th>Title</th>
						<th>SKU</th>
						<th>Price from</th>
						<th>Price to</th>
					</tr>
				</thead>
				<tbody>
					<?php
						$html='';
						foreach ( $info as $fila ){
												
							$html.='<tr>
								<td>'.$fila->{'id'}.'</td>
								<td>'.$fila->{'mainArt_title'}.'</td>
								<td>'.$fila->{'mainArt_sku'}.'</td>
								<td>'.date("Y-m-d", strtotime($fila->{'mainArt_price_from'})).'</td>
								<td>'.date("Y-m-d", strtotime($fila->{'mainArt_price_to'})).'</td>
							</tr>';
						}
						echo $html;
					?>				
				</tbody>
			</table>
		</div>
	</div>
</div>
<br style="clear:both;"/>