<?php
if (isset($this->session->userdata['logged_in'])) {
	$username = ($this->session->userdata['logged_in']['username']);
	$email = ($this->session->userdata['logged_in']['email']);
	$admin = ($this->session->userdata['logged_in']['administrator']);
	
}else{
	header("location: ".base_url()."login/");
}
?>
<div class="header col-md-12">
	<a href="<?php echo base_url() ?>"><div class="header-logo"></div></a>
	<div class="header-info">
		<a href="<?php echo base_url() ?>home/logout/"><div id="logout" align="center"><span class="glyphicon glyphicon-off" aria-hidden="true"></span><br>Log out</div></a>
		<div class="user-data">
			Welcome: <h1><?php echo $username ?></h1>
		</div>
	</div>
	<div class="site-preview"><a href="<?php echo base_url() ?>../" target="_blank"><div class="btn btn-default btn-sm">SEE SITE</div></a></div>
</div>